# Muffin Dev for Node - TS Helpers

This module contains a set of utilities for Typescript.

## Installation

Install it locally with NPM by using the following command:

```bash
npm i @muffin-dev/ts-helpers
```

## Usage

```ts
import * as TSHelpers from '@muffin-dev/ts-helpers';
// OR, if you need only a specific method
// import { enumAsArray } from '@muffin-dev/ts-helpers';

enum DemoEnum {
    A,
    B,
    C
}

const enumArray = TSHelpers.enumAsArray(DemoEnum);
// Contains [ "A", "B", "C" ]
```

## Documentation

[=> See API Documentation](./doc/README.md)