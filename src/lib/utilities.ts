/**
 * Gets an enum as an indexed array.
 * @param enumObj Assumes that the given object is an enum.
 * @returns Returns the resulting array.
 */
export function enumAsArray(enumObj: { [key: string]: unknown }): string[] {
  const keys = Object.keys(enumObj);
  const output = new Array<string>();
  for (const k of keys) {
    if (isNaN(parseInt(k, 10))) {
      output.push(k);
    }
  }
  return output;
}
