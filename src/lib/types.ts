/**
 * Represents a typed "dictionary" (or record), where keys are strings, and values has the given template type.
 * @template TValue The type of each property of this object.
 */
export type TObject<TValue = unknown> = Record<string, TValue>;

/**
 * Represents a constructor method. Note that the class are just a syntactic sugar in JavaScript, and a class name is in fact a constructor
 * method. So you can use this utility type to represent a class name.
 * @template TClass The class of an object created using this constructor.
 */
export type TConstructor<TClass = unknown> = new () => TClass;
