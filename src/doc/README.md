# Muffin Dev for Node - TS Helpers - API Documentation

## Methods

### `enumAsArray()`

```ts
function enumAsArray(enumObj: any): any[]
```

Gets an enum as an indexed array.

- `enumObj: any`: The enum to convert. Assumes that the given object is an enum

Returns the resulting array.

Example:

```ts
import { enumAsArray } from '@muffin-dev/ts-helpers';

enum DemoEnum { A, B, C }
console.log(DemoEnum); // Outputs { "0": "A", "1": "B", "2": "C", A: 0, B: 1, C: 2 }
console.log(enumAsArray(DemoEnum)); // Outputs ["A", "B", "C"]
```

## Types

### `TObject`

```ts
type TObject<TValue = unknown> = Record<string, TValue>
```

Represents a typed "dictionary" (or record), where keys are strings, and values has the given template type. Basically a shortcut for `Record<string, ...>`.

- template `TValue = unknown`: The type of each property of this object

Example:

```ts
import { TObject } from '@muffin-dev/ts-helpers';

const myBooleanDictionary: TObject<boolean> = {
    value1: true,
    value2: false
};

myBooleanDictionary['value1'] = false;
// Outputs { value1: false, value2: false }
console.log(myBooleanDisctionary);
```

### `TConstructor`

```ts
type TConstructor<TClass = unknown> = new () => TClass
```

Represents a constructor method. Note that the class are just a syntactic sugar in JavaScript, and a class name is in fact a constructor method. So you can use this utility type to represent a class name.

- template `TClass = unknown`: The class of an object created using this constructor

Example:

```ts
import { TConstructor } from '@muffin-dev/ts-helpers';

class User {
    username: 'John Doe';
}

const userClass: TConstructor<User> = User;
// Outputs User { username: 'John Doe' }
console.log(new userClass());
```