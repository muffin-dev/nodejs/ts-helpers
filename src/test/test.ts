import { enumAsArray } from '..';

enum DemoEnum {
  A,
  B,
  C
}

console.log('Native enum', DemoEnum);
console.log('Enum as array', enumAsArray(DemoEnum));
